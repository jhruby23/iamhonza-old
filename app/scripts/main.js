//Promise polyfill
if(!window.Promise)
   window.Promise = Promise

//Animate loaded content
let waypoints = []
function showContent(){
   document.getElementsByClassName('header')[0].classList.add('animated', 'fadeIn')
   document.getElementsByClassName('headline')[0].classList.add('animated', 'fadeInUp')
   document.getElementsByClassName('footer')[0].classList.add('animated', 'fadeIn')
   document.getElementsByClassName('footer')[0].style.visibility = 'visible'
   document.getElementById('app').style.visibility = 'visible'

   let sections = document.getElementsByClassName('section')
   for(let i = 0; i < sections.length; i++){
      waypoints[i] = new Waypoint({
         element: sections[i],
         offset: '100%',
         handler: () => {
            sections[i].getElementsByClassName('section__name')[0].classList.add('animated', 'fadeInLeft')
            sections[i].getElementsByClassName('section__list')[0].classList.add('animated', 'fadeInRight')
         }
      })
   }
}

//Wait for the primary font to load
let font = new FontFaceObserver('Titillium Web').load()

//Load and change page content
function changePage(name){
   //Hide old content
   document.getElementsByClassName('footer')[0].style.visibility = 'hidden'
   document.getElementById('app').style.visibility = 'hidden'
   window.scrollTo(0, 0)

   //Change navigation active link
   let nav = document.getElementsByClassName('header__nav__link')
   for(let i = 0; i < nav.length; i++)
      nav[i].classList.remove('header__nav__link--active')
   let active = document.getElementById('nav_' + name)
   if(active)
      active.classList.add('header__nav__link--active')

   //Change page title
   let title = name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
      return letter.toUpperCase()
   })
   document.title = title + ' | I am Honza'

   //Send pageview to google analytics
   ga('set', {
      page: location.pathname,
      title: title
   })
   ga('send', 'pageview')

   //Destroy old waypoints
   for(let i = 0; i < waypoints.length; i++)
      waypoints[i].destroy()

   //Fetch new content
   let page = axios.get('layout_' + name + '.html')

   //When both font and content are properly loaded, show the content
   Promise.all([font, page]).then(res => {
      document.documentElement.classList.add('fonts-loaded')
      document.getElementById('app').innerHTML = res[1].data
      showContent()
      router.updatePageLinks()
   })
}

//Navigo router
let router = new Navigo(location.protocol + '//' + location.host, false)
router.notFound((query) => {
   changePage('404')
})
.on({
   'work': () => {
      changePage('work')
   },
   'about': () => {
      changePage('about')
   },
   'contact': () => {
      changePage('contact')
   }
})
.on(() => {
   changePage('intro')
})
.resolve()
